import { Component } from '@angular/core';


@Component({
  selector: 'app-buzzwords',
  templateUrl: './buzzwords.component.html',
  styleUrls: ['./buzzwords.component.scss']
})
export class BuzzwordsComponent  {

  showBuzzword : boolean = false; 
  technologieName : string = ''; 

  constructor() {}

  generateBuzzword(){

    this.showBuzzword = true; 

  }

  
}
